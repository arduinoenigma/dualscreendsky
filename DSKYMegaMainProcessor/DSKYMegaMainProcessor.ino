// DSKYMegaMainProcessor.ino
// @arduinoenigma 2021
//
// this sketch runs on the Arduino Mega
// reads built in keyboard and sends keys to Serial (USB) and Serial3 (spare)
// incoming command characters are read from Serial or Serial3 and forwarded to Screens on Serial1 and Serial2

#include "GPIO.h"

#define HWSERIALSPEED 9600
#define SWSERIALSPEED 38400

#define KBUFFERSIZE 300
#define KOUTSPEED 7000

#define DEBOUNCEVAL 20
#define DEBOUNCEVAL1 16

GPIO<BOARD::D13> LAMP;

GPIO<BOARD::D47> KEYROW1;
GPIO<BOARD::D46> KEYROW2;
GPIO<BOARD::D44> KEYROW3;

GPIO<BOARD::D48> KEYCOL1;
GPIO<BOARD::D50> KEYCOL2;
GPIO<BOARD::D52> KEYCOL3;
GPIO<BOARD::D53> KEYCOL4;
GPIO<BOARD::D51> KEYCOL5;
GPIO<BOARD::D30> KEYCOL6;
GPIO<BOARD::D31> KEYCOL7;

// VN+-0123456789CPKER

static char Keys[21] = { 'V', 'N', ' ', '+', '-', '0', '7', '4', '1', '8', '5', '2', '9', '6', '3', 'C', 'P', 'K', 'E', 'R', ' ' };
byte KeysDebounce[21] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

byte KBuffer[KBUFFERSIZE];
byte KBufferIn = 0;
byte KBufferOut = 0;
unsigned long KBufferTimer = 0;

//
void initLamp() /*xls*/
{
  LAMP.low();
  LAMP.output();
}


void allRowsInput() {
  KEYROW1.input();
  KEYROW2.input();
  KEYROW3.input();

  KEYROW1.high();
  KEYROW2.high();
  KEYROW3.high();
}


void allColsInput() {
  KEYCOL1.input();
  KEYCOL2.input();
  KEYCOL3.input();
  KEYCOL4.input();
  KEYCOL5.input();
  KEYCOL6.input();
  KEYCOL7.input();

  KEYCOL1.high();
  KEYCOL2.high();
  KEYCOL3.high();
  KEYCOL4.high();
  KEYCOL5.high();
  KEYCOL6.high();
  KEYCOL7.high();
}


void selectRow(byte row) {
  allRowsInput();

  switch (row) {
    case 1:
      {
        KEYROW1.output();
        KEYROW1.low();
        break;
      }
    case 2:
      {
        KEYROW2.output();
        KEYROW2.low();
        break;
      }
    case 3:
      {
        KEYROW3.output();
        KEYROW3.low();
        break;
      }
  }

  //delayMicroseconds(2); // allow pin to go low so it can be read
}


byte readCol(byte col) {
  switch (col) {
    case 1:
      {
        return KEYCOL1.read();  //break not needed, return exits, compiler ignores break anyways
      }
    case 2:
      {
        return KEYCOL2.read();
      }
    case 3:
      {
        return KEYCOL3.read();
      }
    case 4:
      {
        return KEYCOL4.read();
      }
    case 5:
      {
        return KEYCOL5.read();
      }
    case 6:
      {
        return KEYCOL6.read();
      }
    case 7:
      {
        return KEYCOL7.read();
      }
    default:
      {
        return 1;
      }
  }
}


//
// 'V', 'N', ' ', '+', '-', '0', '7', '4', '1', '8', '5', '2', '9', '6', '3', 'C', 'P', 'K', 'E', 'R', ' '};
//
// V+789CE
// N-456PR
//  0123K
//

byte readKey(byte key) {
  switch (key) {
    //V
    case 0:
      {
        selectRow(1);
        return readCol(1);
      }
    //N
    case 1:
      {
        selectRow(2);
        return readCol(1);
      }
    //+
    case 3:
      {
        selectRow(1);
        return readCol(2);
      }
    case 4:
      {
        selectRow(2);
        return readCol(2);
      }
    case 5:
      {
        selectRow(3);
        return readCol(2);
      }

    //7
    case 6:
      {
        selectRow(1);
        return readCol(3);
      }
    case 7:
      {
        selectRow(2);
        return readCol(3);
      }
    case 8:
      {
        selectRow(3);
        return readCol(3);
      }
    //8
    case 9:
      {
        selectRow(1);
        return readCol(4);
      }
    case 10:
      {
        selectRow(2);
        return readCol(4);
      }
    case 11:
      {
        selectRow(3);
        return readCol(4);
      }
    //9
    case 12:
      {
        selectRow(1);
        return readCol(5);
      }
    case 13:
      {
        selectRow(2);
        return readCol(5);
      }
    case 14:
      {
        selectRow(3);
        return readCol(5);
      }
    //CPK
    case 15:
      {
        selectRow(1);
        return readCol(6);
      }
    case 16:
      {
        selectRow(2);
        return readCol(6);
      }
    case 17:
      {
        selectRow(3);
        return readCol(6);
      }
    //ER
    case 18:
      {
        selectRow(1);
        return readCol(7);
      }
    case 19:
      {
        selectRow(2);
        return readCol(7);
      }
    default:
      {
        return 1;
      }
  }
}


void processSwap() {
  static byte swapdebounce = 0;

  static byte arrangement = 0;  // bug: unify this into a single function and read/write to eeprom

  if ((KeysDebounce[1] > DEBOUNCEVAL1) && (KeysDebounce[19] > DEBOUNCEVAL1)) {
    if (swapdebounce == 0) {
      //Serial.println("Swap");

      Serial1.begin(SWSERIALSPEED);
      Serial2.begin(SWSERIALSPEED);

      delay(10);

      if (arrangement == 0) {
        Serial1.print(F("]!"));
        Serial2.print(F("[!"));
        arrangement = 1;
      } else {
        Serial2.print(F("]!"));
        Serial1.print(F("[!"));
        arrangement = 0;
      }
    }
    swapdebounce = DEBOUNCEVAL;
  }

  if (swapdebounce) {
    swapdebounce--;
  }
}


void processKeyboard() {
  static byte key = 0;

  if (readKey(key) == 0) {
    if (KeysDebounce[key] == 0) {
      Serial.print(Keys[key]);
      Serial3.print(Keys[key]);
    }
    KeysDebounce[key] = DEBOUNCEVAL;
  }

  if (KeysDebounce[key] > 0) {
    KeysDebounce[key]--;
  }

  key++;
  if (key == 20) {
    key = 0;
  }

  processSwap();
}


void sendSimulated() {
  static byte k;
  static long delay = 0;

  delay++;

  //if (delay == 200000)
  if (delay == 8000) {
    delay = 0;

    k++;

    switch (k) {
      case 1:
        {
          Serial.print('V');

          //BUG: disable these two
          //Serial1.print(F("A")); // activate indicator on LCD Screen
          //Serial2.print(F("A")); // proves that comm to screens is working
          //LAMP.high();

          break;
        }
      case 2:
        {
          Serial.print('3');
          break;
        }
      case 3:
        {
          Serial.print('5');

          //BUG: disable these two:
          //Serial1.print(F("a"));
          //Serial2.print(F("a"));
          //LAMP.low();

          break;
        }
      case 4:
        {
          Serial.print('E');
          k = 0;
          break;
        }
    }
  }
}


//BUG: call this function from keyboardread... setup does not init ports until something is read...
void initScreenPorts() {
  Serial1.begin(SWSERIALSPEED);
  Serial2.begin(SWSERIALSPEED);

  //BUG: read these settings from EEPROM
  Serial1.print(F("]!"));
  Serial2.print(F("[!"));
}


void setup() {
  // put your setup code here, to run once:

  byte K;
  byte init = 0;

  Serial.begin(HWSERIALSPEED);
  Serial3.begin(HWSERIALSPEED);
  //Serial.print(F("MegaMainProcessor")); //BUG: DISABLE, for testing only

  initLamp();

  allRowsInput();
  allColsInput();

  // wait for a command and then initialize screen serial ports
  // simplifies main loop.
  do {
    processKeyboard();
    K = Serial.read() & Serial3.read();

    if ((K != 255) && (K > 31) && (K != '[') && (K != ']')) {
      if ((K | 32) > ('a' - 1)) {
        Serial1.begin(SWSERIALSPEED);
        Serial2.begin(SWSERIALSPEED);

        //BUG: read these settings from EEPROM
        Serial1.print(F("]!"));
        Serial2.print(F("[!"));

        Serial1.write(K);
        Serial2.write(K);

        //LAMP.high();
        init = 1;
      }
    }
  } while (init == 0);
}

// send this string using the serial monitor to turn all segments on and off
// ABCDEFGHIJKLMOQP99V12N34X+12345Y+12345Z+12345abcdefghijklmoqP  V  N  X      Y      Z 
// in AGCjs, Verb 16 Noun 65 Enter is a hard test as ACTY is turned on and off and Z is updated     

void loop() {
  // put your main code here, to run repeatedly:
  static byte init = 0;

  unsigned long KBufferNow = micros();
  byte K;
  byte KOut;

  processKeyboard();
  K = Serial.read() & Serial3.read();

  if ((KBufferIn != KBufferOut) && ((KBufferNow - KBufferTimer) > KOUTSPEED)) {
    KBufferTimer = KBufferNow;
    KOut = KBuffer[KBufferOut];
    KBufferOut++;
    if (KBufferOut == KBUFFERSIZE) {
      KBufferOut = 0;
    }

    Serial1.write(KOut);
    Serial2.write(KOut);
  }

  if ((K != 255) && (K > 31) && (K != '[') && (K != ']')) {
    //BUG: Instead of immediately sending key out to screen serial ports, buffer it
    //Serial1.write(K);
    //Serial2.write(K);

    KBuffer[KBufferIn] = K;
    KBufferIn++;
    if (KBufferIn == KBUFFERSIZE) {
      KBufferIn = 0;
    }

    if (K == 'I') {
      LAMP.high();
    }

    if (K == 'i') {
      LAMP.low();
    }
  }
}
