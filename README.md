# DualScreenDSKY

This project contains the two Arduino sketches that power the Dual Screen Apollo DSKY
https://arduinoenigma.blogspot.com/p/dual-screen-apollo-dsky.html

This DSKY works with SerialMoonJS, a modified Apollo Guidance Computer Simulator.
https://gitlab.com/arduinoenigma/serialmoonjs

Download both folders DSKYMegaMainProcessor and DSKYUniversalScreen to your Arduino Folder.

The following libraries must be installed in the Arduino Libraries folder:
https://gitlab.com/arduinoenigma/Arduino-GPIO 
https://gitlab.com/arduinoenigma/FastSeedTFTv2
