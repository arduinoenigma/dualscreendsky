// DSKYUniversalScreen.ino - LeftScreen.ino
// @arduinoenigma 2021


byte L_indicatorstatus[15] = {255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255};


void L_DrawIndicator(byte indicator, byte state) /* XLS */
{
  byte x1, x2;
  int  y1, y2;
  int  color;
  int  drawc;
  byte good = 1;

  const __FlashStringHelper *Line1;
  const __FlashStringHelper *Line2 = 0;

  byte L1x;
  byte L2x;

  x2 = 117;
  y2 = 40;

  switch (indicator)
  {
    case 0:
      {
        x1 = 0;
        y1 = 0;
        color = WHITE;
        Line1 = F("UPLINK");
        Line2 = F("ACTY");
        L1x = 16;
        L2x = 31;
        break;
      }

    case 1:
      {
        x1 = 123;
        y1 = 0;
        color = AVYELLOW;
        Line1 = F("TEMP");
        L1x = 31;
        break;
      }

    case 2:
      {
        x1 = 0;
        y1 = 46;
        color = WHITE;
        Line1 = F("NO ATT");
        L1x = 16;
        break;
      }

    case 3:
      {
        x1 = 123;
        y1 = 46;
        color = AVYELLOW;
        Line1 = F("GIMBAL");
        Line2 = F("LOCK");
        L1x = 16;
        L2x = 31;
        break;
      }

    case 4:
      {
        x1 = 0;
        y1 = 92;
        color = WHITE;
        Line1 = F("STBY");
        L1x = 31;
        break;
      }

    case 5:
      {
        x1 = 123;
        y1 = 92;
        color = AVYELLOW;
        Line1 = F("PROG");
        L1x = 31;
        break;
      }

    case 6:
      {
        x1 = 0;
        y1 = 138;
        color = WHITE;
        Line1 = F("KEY REL");
        L1x = 8;
        break;
      }

    case 7:
      {
        x1 = 123;
        y1 = 138;
        color = AVYELLOW;
        Line1 = F("RESTART");
        L1x = 8;
        break;
      }

    case 8:
      {
        x1 = 0;
        y1 = 184;
        color = WHITE;
        Line1 = F("OPR ERR");
        L1x = 8;
        break;
      }

    case 9:
      {
        x1 = 123;
        y1 = 184;
        color = AVYELLOW;
        Line1 = F("TRACKER");
        L1x = 8;
        break;
      }

    case 10:
      {
        x1 = 0;
        y1 = 230;
        color = WHITE;
        #if defined MOONLANDER
        Line1 = F("DAP NOT");
        Line2 = F("IN CTRL");
        L1x = 8;
        L2x = 8;
        #else
        Line1 = F("");
        #endif
        break;
      }

    case 11:
      {
        x1 = 123;
        y1 = 230;
        color = AVYELLOW;
        Line1 = F("ALT");
        L1x = 38;
        break;
      }

    case 12:
      {
        x1 = 0;
        y1 = 276;
        color = WHITE;
        #if defined MOONLANDER
        Line1 = F("PRIORITY");
        Line2 = F("DISPLAY");
        L1x = 1;
        L2x = 8;
        #else
        Line1 = F("");
        #endif
        break;
      }

    case 14:
      {
        x1 = 123;
        y1 = 276;
        color = AVYELLOW;
        Line1 = F("VEL");
        L1x = 38;
        break;
      }

    default:
      {
        good = 0;
        break;
      }
  }

  if (good)
  {
    if (state)
    {
      drawc = color;
    }
    else
    {
      drawc = GRAY3;
    }

    if (L_indicatorstatus[indicator] != state)
    {
      BothTft.fillRectangle(x1, y1, x2, y2, drawc);

      if (Line2)
      {
        DrawText(x1 + L1x, y1 + 5, Line1);
        DrawText(x1 + L2x, y1 + 22, Line2);
      }
      else
      {
        DrawText(x1 + L1x, y1 + 14, Line1);
      }

      L_indicatorstatus[indicator] = state;
    }
  }
}


void L_ClearAllIndicators(byte state = 0) /* XLS */
{
  for (byte i = 0; i < 15; i++)
  {
    L_indicatorstatus[i] = 255;
    L_DrawIndicator(i, state);
  }
}


// ON/OFF
// ABCDEFGHIJKLM O abcdefghijklm o
void L_ReadCmd() /* XLS */
{
  byte K, k;

  static byte screen = 0;
  static byte nextb = 0;

  K = Serial.read() & mySerial.read();  // read both and mix with (&). K maybe uppercase

  k = K | 32;         // added 32, definitely lowercase

  if ((k > 96) && (k < 96 + 16)) // A through O
  {
    //A..O gets turned to 0..15
    //if A..N, state is 32(on)
    //if a..n, state is 0 (off)
    L_DrawIndicator(k - 97, (K ^ 32) & 32);
  }

  //order is important
  // do 1st
  if ((screen != 0) && (K == '!'))
  {
    selectedscreen = screen;
    EEPROM.write(0, selectedscreen);

    screen = 0;
    nextb = 0;

    R_setup();
  }

  // do 2nd
  if ((nextb) && (K != 255))
  {
    screen = 0;
    nextb = 0;
  }

  // do 3rd
  if (K == ']')
  {
    screen = K;
    nextb = 1;
  }
}


void L_setup() /* XLS */
{
  BothTft.fillRectangle(0, 0, 240, 320, BLACK);
  L_ClearAllIndicators();
}

