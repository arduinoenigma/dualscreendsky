// DSKYUniversalScreen.ino - Common.ino
// @arduinoenigma 2021


void DrawText(byte x1, int y1, const __FlashStringHelper *Text) /* XLS */
{
  byte cx;
  INT16U p = 0;
  INT8U x = 0;
  INT16U y = 0;

  const char *t = (const char *)Text;

  BothTft.setWide();

  do
  {
    cx = pgm_read_byte(t + p++);

    if (cx != 0)
    {
      BothTft.drawChar(cx, x1 + x++ * 15, y1 + y , 2, BLACK);
    }
    else
    {
      break;
    }

  } while (1);

  BothTft.setBold();
}

