// DSKYUniversalScreen.ino - Tests.ino
// @arduinoenigma 2021


void R_TestAllDigits() /* XLS */
{
  for (byte i = 0; i < 10; i++)
  {
    R_Draw7Seg(0 + i * 20, 5, i, 255);
  }
}


void R_DrawDummyDigits() /* XLS */
{
  R_Draw7Seg(130, 22, 8, 255);
  R_Draw7Seg(130, 94, 8, 255);
  R_Draw7Seg(130, 155, 8, 255);
  R_Draw7Seg(130, 212, 8, 255);
  R_Draw7Seg(130, 270, 8, 255);
}


void R_DrawAllDigits() /* XLS */
{
  byte sp = 37;

  for (byte i = 0; i < 2; i++)
  {
    R_Draw7Seg(134 + i * sp, 22, 8, 255); // PROG
    R_Draw7Seg(20 + i * sp, 94, 8, 255); // VERB
    R_Draw7Seg(134 + i * sp, 94, 8, 255); // NOUN
  }

  for (byte i = 0; i < 5; i++)
  {
    R_Draw7Seg(35 + i * sp, 155, 8, 255); // R1
    R_Draw7Seg(35 + i * sp, 212, 8, 255); // R2
    R_Draw7Seg(35 + i * sp, 270, 8, 255); // R3
  }
}

