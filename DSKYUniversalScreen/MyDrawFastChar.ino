// DSKYUniversalScreen.ino - MyDrawFastChar.ino
// @arduinoenigma 2021

/*

  void myDrawFastChar(uint8_t ascii, uint8_t poX, uint16_t poY, uint8_t size, uint16_t fgcolor)
  {
  switch (ascii)
  {
    // A
    case 65:
      {
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 2, poY + 7, fgcolor);
        BothTft.setPixel(poX + 2, poY + 8, fgcolor);
        BothTft.setPixel(poX + 2, poY + 9, fgcolor);
        BothTft.setPixel(poX + 2, poY + 10, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 2, fgcolor);
        BothTft.setPixel(poX + 3, poY + 3, fgcolor);
        BothTft.setPixel(poX + 3, poY + 4, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 9, fgcolor);
        BothTft.setPixel(poX + 3, poY + 10, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 10, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 2, fgcolor);
        BothTft.setPixel(poX + 6, poY + 3, fgcolor);
        BothTft.setPixel(poX + 6, poY + 4, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 3, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 7, poY + 7, fgcolor);
        BothTft.setPixel(poX + 7, poY + 8, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // B
    case 66:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 13, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 2, fgcolor);
        BothTft.setPixel(poX + 6, poY + 4, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 7, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 3, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 7, poY + 7, fgcolor);
        BothTft.setPixel(poX + 7, poY + 8, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        break;
      }

    // C
    case 67:
      {
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 2, poY + 9, fgcolor);
        BothTft.setPixel(poX + 2, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        break;
      }

    // E
    case 69:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 13, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // G
    case 71:
      {
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 2, poY + 9, fgcolor);
        BothTft.setPixel(poX + 2, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 8, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        break;
      }

    // I
    case 73:
      {
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 2, fgcolor);
        BothTft.setPixel(poX + 4, poY + 3, fgcolor);
        BothTft.setPixel(poX + 4, poY + 4, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 7, fgcolor);
        BothTft.setPixel(poX + 4, poY + 8, fgcolor);
        BothTft.setPixel(poX + 4, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 10, fgcolor);
        BothTft.setPixel(poX + 4, poY + 11, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 2, fgcolor);
        BothTft.setPixel(poX + 5, poY + 3, fgcolor);
        BothTft.setPixel(poX + 5, poY + 4, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 5, poY + 11, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        break;
      }

    // K
    case 75:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 4, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 7, fgcolor);
        BothTft.setPixel(poX + 4, poY + 3, fgcolor);
        BothTft.setPixel(poX + 4, poY + 4, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 7, fgcolor);
        BothTft.setPixel(poX + 4, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 2, fgcolor);
        BothTft.setPixel(poX + 5, poY + 3, fgcolor);
        BothTft.setPixel(poX + 5, poY + 4, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 2, fgcolor);
        BothTft.setPixel(poX + 6, poY + 3, fgcolor);
        BothTft.setPixel(poX + 6, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // L
    case 76:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 13, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // M
    case 77:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 7, fgcolor);
        BothTft.setPixel(poX + 3, poY + 8, fgcolor);
        BothTft.setPixel(poX + 3, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 10, fgcolor);
        BothTft.setPixel(poX + 4, poY + 11, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 5, poY + 11, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 7, fgcolor);
        BothTft.setPixel(poX + 6, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        BothTft.setPixel(poX + 9, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 6, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // N
    case 78:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 3, poY + 2, fgcolor);
        BothTft.setPixel(poX + 3, poY + 3, fgcolor);
        BothTft.setPixel(poX + 3, poY + 4, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 3, fgcolor);
        BothTft.setPixel(poX + 4, poY + 4, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 6, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 10, fgcolor);
        BothTft.setPixel(poX + 6, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        BothTft.setPixel(poX + 9, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 6, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // O
    case 79:
      {
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 2, poY + 9, fgcolor);
        BothTft.setPixel(poX + 2, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 2, fgcolor);
        BothTft.setPixel(poX + 3, poY + 11, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 2, fgcolor);
        BothTft.setPixel(poX + 6, poY + 11, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 3, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 6, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        break;
      }

    // P
    case 80:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        break;
      }

    // R
    case 82:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 0, poY + 12, fgcolor);
        BothTft.setPixel(poX + 0, poY + 13, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 1, poY + 13, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 7, fgcolor);
        BothTft.setPixel(poX + 6, poY + 8, fgcolor);
        BothTft.setPixel(poX + 6, poY + 9, fgcolor);
        BothTft.setPixel(poX + 6, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 8, poY + 13, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        BothTft.setPixel(poX + 9, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 13, fgcolor);
        break;
      }

    // S
    case 83:
      {
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 2, poY + 5, fgcolor);
        BothTft.setPixel(poX + 2, poY + 6, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 13, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 3, poY + 6, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 6, poY + 6, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 6, fgcolor);
        BothTft.setPixel(poX + 7, poY + 7, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        break;
      }

    // T
    case 84:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 2, poY + 0, fgcolor);
        BothTft.setPixel(poX + 2, poY + 1, fgcolor);
        BothTft.setPixel(poX + 3, poY + 0, fgcolor);
        BothTft.setPixel(poX + 3, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 0, fgcolor);
        BothTft.setPixel(poX + 4, poY + 1, fgcolor);
        BothTft.setPixel(poX + 4, poY + 2, fgcolor);
        BothTft.setPixel(poX + 4, poY + 3, fgcolor);
        BothTft.setPixel(poX + 4, poY + 4, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 7, fgcolor);
        BothTft.setPixel(poX + 4, poY + 8, fgcolor);
        BothTft.setPixel(poX + 4, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 10, fgcolor);
        BothTft.setPixel(poX + 4, poY + 11, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 0, fgcolor);
        BothTft.setPixel(poX + 5, poY + 1, fgcolor);
        BothTft.setPixel(poX + 5, poY + 2, fgcolor);
        BothTft.setPixel(poX + 5, poY + 3, fgcolor);
        BothTft.setPixel(poX + 5, poY + 4, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 5, poY + 11, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 0, fgcolor);
        BothTft.setPixel(poX + 6, poY + 1, fgcolor);
        BothTft.setPixel(poX + 7, poY + 0, fgcolor);
        BothTft.setPixel(poX + 7, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        break;
      }

    // U
    case 85:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 0, poY + 10, fgcolor);
        BothTft.setPixel(poX + 0, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 1, poY + 11, fgcolor);
        BothTft.setPixel(poX + 1, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 2, poY + 12, fgcolor);
        BothTft.setPixel(poX + 2, poY + 13, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 3, poY + 13, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 6, poY + 13, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 7, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 13, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 8, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 12, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        BothTft.setPixel(poX + 9, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 6, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        BothTft.setPixel(poX + 9, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 11, fgcolor);
        break;
      }

    // V
    case 86:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 0, poY + 3, fgcolor);
        BothTft.setPixel(poX + 0, poY + 4, fgcolor);
        BothTft.setPixel(poX + 0, poY + 5, fgcolor);
        BothTft.setPixel(poX + 0, poY + 6, fgcolor);
        BothTft.setPixel(poX + 0, poY + 7, fgcolor);
        BothTft.setPixel(poX + 0, poY + 8, fgcolor);
        BothTft.setPixel(poX + 0, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 1, poY + 4, fgcolor);
        BothTft.setPixel(poX + 1, poY + 5, fgcolor);
        BothTft.setPixel(poX + 1, poY + 6, fgcolor);
        BothTft.setPixel(poX + 1, poY + 7, fgcolor);
        BothTft.setPixel(poX + 1, poY + 8, fgcolor);
        BothTft.setPixel(poX + 1, poY + 9, fgcolor);
        BothTft.setPixel(poX + 1, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 9, fgcolor);
        BothTft.setPixel(poX + 2, poY + 10, fgcolor);
        BothTft.setPixel(poX + 2, poY + 11, fgcolor);
        BothTft.setPixel(poX + 3, poY + 10, fgcolor);
        BothTft.setPixel(poX + 3, poY + 11, fgcolor);
        BothTft.setPixel(poX + 3, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 11, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 11, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 10, fgcolor);
        BothTft.setPixel(poX + 6, poY + 11, fgcolor);
        BothTft.setPixel(poX + 6, poY + 12, fgcolor);
        BothTft.setPixel(poX + 7, poY + 9, fgcolor);
        BothTft.setPixel(poX + 7, poY + 10, fgcolor);
        BothTft.setPixel(poX + 7, poY + 11, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 8, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 5, fgcolor);
        BothTft.setPixel(poX + 8, poY + 6, fgcolor);
        BothTft.setPixel(poX + 8, poY + 7, fgcolor);
        BothTft.setPixel(poX + 8, poY + 8, fgcolor);
        BothTft.setPixel(poX + 8, poY + 9, fgcolor);
        BothTft.setPixel(poX + 8, poY + 10, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        BothTft.setPixel(poX + 9, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 4, fgcolor);
        BothTft.setPixel(poX + 9, poY + 5, fgcolor);
        BothTft.setPixel(poX + 9, poY + 6, fgcolor);
        BothTft.setPixel(poX + 9, poY + 7, fgcolor);
        BothTft.setPixel(poX + 9, poY + 8, fgcolor);
        BothTft.setPixel(poX + 9, poY + 9, fgcolor);
        break;
      }

    // Y
    case 89:
      {
        BothTft.setPixel(poX + 0, poY + 0, fgcolor);
        BothTft.setPixel(poX + 0, poY + 1, fgcolor);
        BothTft.setPixel(poX + 0, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 0, fgcolor);
        BothTft.setPixel(poX + 1, poY + 1, fgcolor);
        BothTft.setPixel(poX + 1, poY + 2, fgcolor);
        BothTft.setPixel(poX + 1, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 2, fgcolor);
        BothTft.setPixel(poX + 2, poY + 3, fgcolor);
        BothTft.setPixel(poX + 2, poY + 4, fgcolor);
        BothTft.setPixel(poX + 3, poY + 3, fgcolor);
        BothTft.setPixel(poX + 3, poY + 4, fgcolor);
        BothTft.setPixel(poX + 3, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 4, fgcolor);
        BothTft.setPixel(poX + 4, poY + 5, fgcolor);
        BothTft.setPixel(poX + 4, poY + 6, fgcolor);
        BothTft.setPixel(poX + 4, poY + 7, fgcolor);
        BothTft.setPixel(poX + 4, poY + 8, fgcolor);
        BothTft.setPixel(poX + 4, poY + 9, fgcolor);
        BothTft.setPixel(poX + 4, poY + 10, fgcolor);
        BothTft.setPixel(poX + 4, poY + 11, fgcolor);
        BothTft.setPixel(poX + 4, poY + 12, fgcolor);
        BothTft.setPixel(poX + 4, poY + 13, fgcolor);
        BothTft.setPixel(poX + 5, poY + 4, fgcolor);
        BothTft.setPixel(poX + 5, poY + 5, fgcolor);
        BothTft.setPixel(poX + 5, poY + 6, fgcolor);
        BothTft.setPixel(poX + 5, poY + 7, fgcolor);
        BothTft.setPixel(poX + 5, poY + 8, fgcolor);
        BothTft.setPixel(poX + 5, poY + 9, fgcolor);
        BothTft.setPixel(poX + 5, poY + 10, fgcolor);
        BothTft.setPixel(poX + 5, poY + 11, fgcolor);
        BothTft.setPixel(poX + 5, poY + 12, fgcolor);
        BothTft.setPixel(poX + 5, poY + 13, fgcolor);
        BothTft.setPixel(poX + 6, poY + 3, fgcolor);
        BothTft.setPixel(poX + 6, poY + 4, fgcolor);
        BothTft.setPixel(poX + 6, poY + 5, fgcolor);
        BothTft.setPixel(poX + 7, poY + 2, fgcolor);
        BothTft.setPixel(poX + 7, poY + 3, fgcolor);
        BothTft.setPixel(poX + 7, poY + 4, fgcolor);
        BothTft.setPixel(poX + 8, poY + 0, fgcolor);
        BothTft.setPixel(poX + 8, poY + 1, fgcolor);
        BothTft.setPixel(poX + 8, poY + 2, fgcolor);
        BothTft.setPixel(poX + 8, poY + 3, fgcolor);
        BothTft.setPixel(poX + 9, poY + 0, fgcolor);
        BothTft.setPixel(poX + 9, poY + 1, fgcolor);
        BothTft.setPixel(poX + 9, poY + 2, fgcolor);
        break;
      }

    default:
      {
        //BothTft.drawChar(ascii, poX, poY, size, fgcolor);
        BothTft.drawChar(ascii, poX, poY, size, RED);
        break;
      }
  }
  }


  void DrawText(byte x1, int y1, const __FlashStringHelper *Text)
  {
  byte cx;
  INT16U p = 0;
  INT8U x = 0;
  INT16U y = 0;

  const char *t = (const char *)Text;

  BothTft.setWide();

  do
  {
    cx = pgm_read_byte(t + p++);

    if (cx != 0)
    {
      myDrawFastChar(cx, x1 + x++ * 15, y1 + y , 2, BLACK);
    }
    else
    {
      break;
    }

  } while (1);

  BothTft.setBold();
  }

*/

