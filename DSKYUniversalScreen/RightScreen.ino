// DSKYUniversalScreen.ino - RightScreen.ino
// @arduinoenigma 2021


#define RawSBitsB 0b00000000
#define RawSBitsN 0b00000001
#define RawSBitsP 0b00000011

#define RawBitsB 0b00000000
#define RawBits0 0b00111111
#define RawBits1 0b00000110
#define RawBits2 0b01011011
#define RawBits3 0b01001111
#define RawBits4 0b01100110
#define RawBits5 0b01101101
#define RawBits6 0b01111101
#define RawBits7 0b00000111
#define RawBits8 0b01111111
#define RawBits9 0b01101111

byte R_indicatorstatus[17] = {255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255};

byte R_PV[2] = {128, 128};
byte R_VV[2] = {128, 128};
byte R_NV[2] = {128, 128};
byte R_AV[6] = {128, 128, 128, 128, 128, 128};
byte R_BV[6] = {128, 128, 128, 128, 128, 128};
byte R_CV[6] = {128, 128, 128, 128, 128, 128};


byte R_GetRawSignBits(byte sign) /* XLS */
{
  switch (sign)
  {
    case '-':
      {
        return RawSBitsN;
      }

    case '+':
      {
        return RawSBitsP;
      }

    default:
      {
        return RawSBitsB;
      }
  }
}


byte R_GetRawDigitBits(byte digit) /* XLS */
{
  switch (digit)
  {
    case 0:
      {
        return RawBits0;
      }
    case 1:
      {
        return RawBits1;
      }
    case 2:
      {
        return RawBits2;
      }
    case 3:
      {
        return RawBits3;
      }
    case 4:
      {
        return RawBits4;
      }
    case 5:
      {
        return RawBits5;
      }
    case 6:
      {
        return RawBits6;
      }
    case 7:
      {
        return RawBits7;
      }
    case 8:
      {
        return RawBits8;
      }
    case 9:
      {
        return RawBits9;
      }
    default :
      {
        return RawBitsB;
      }
  }
}


#define SegSize 21
#define SegWidth 3
#define Slant 5
#define ONCOLOR GREEN
#define OFFCOLOR BLACK
#define maska 0b00000001
#define maskb 0b00000010
#define maskc 0b00000100
#define maskd 0b00001000
#define maske 0b00010000
#define maskf 0b00100000
#define maskg 0b01000000

#define NegSize 15
#define PosSize 6
#define maskn 0b00000001
#define maskp 0b00000010


void R_DrawSign(byte x1, int y1, byte sign, byte prevsign) /* XLS */
{
  byte raw = R_GetRawSignBits(sign);
  byte prevraw = R_GetRawSignBits(prevsign);
  int color;

  byte segn = raw & maskn;
  byte segp = raw & maskp;

  //+
  if ((segp) != (prevraw & maskp))
  {
    if (segp)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    BothTft.fillRectangle(x1 + PosSize, y1 + SegSize - PosSize, SegWidth, PosSize, color);
    BothTft.fillRectangle(x1 + PosSize, y1 + SegSize + SegWidth, SegWidth, PosSize, color);
  }

  //-
  if ((segn) != (prevraw & maskn))
  {
    if (segn)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    BothTft.fillRectangle(x1, y1 + SegSize, NegSize, SegWidth, color);
  }
}


void R_Draw7Seg(byte x1, int y1, byte digit, byte prevdigit) /* XLS */
{
  byte raw = R_GetRawDigitBits(digit);
  byte prevraw = R_GetRawDigitBits(prevdigit);
  int color;

  byte sega = raw & maska;
  byte segb = raw & maskb;
  byte segc = raw & maskc;
  byte segd = raw & maskd;
  byte sege = raw & maske;
  byte segf = raw & maskf;
  byte segg = raw & maskg;

  byte xa, xb;
  int  ya, yb;

  //a
  if ((sega) != (prevraw & maska))
  {
    if (sega)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    BothTft.fillRectangle(x1 + Slant + Slant, y1, SegSize + SegWidth, SegWidth, color);
  }

  //b
  if ((segb) != (prevraw & maskb))
  {
    if (segb)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    //BothTft.fillRectangle(x1 + SegSize, y1, SegWidth , SegSize , color);
    xa = x1 + SegSize + Slant + Slant;
    xb = x1 + SegSize + Slant;
    yb = y1 + SegSize + SegWidth;
    BothTft.drawLine(xa++, y1, xb++, yb, color);
    BothTft.drawLine(xa++, y1, xb++, yb, color);
    BothTft.drawLine(xa++, y1, xb++, yb, color);
    BothTft.drawLine(xa++, y1, xb++, yb, color);
  }

  //c
  if ((segc) != (prevraw & maskc))
  {
    if (segc)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    //BothTft.fillRectangle(x1 + SegSize, y1 + SegSize, SegWidth, SegSize, color);
    xa = x1 + SegSize + Slant;
    xb = x1 + SegSize;
    ya = y1 + SegSize;
    yb = y1 + SegSize + SegSize + SegWidth;
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
  }

  //d
  if ((segd) != (prevraw & maskd))
  {
    if (segd)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    BothTft.fillRectangle(x1, y1 + SegSize + SegSize, SegSize + SegWidth, SegWidth , color);
  }

  //e
  if ((sege) != (prevraw & maske))
  {
    if (sege)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    //BothTft.fillRectangle(x1, y1 + SegSize , SegWidth , SegSize, color);
    xa = x1 + Slant;
    xb = x1;
    ya = y1 + SegSize;
    yb = y1 + SegSize + SegSize;
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
  }

  //f
  if ((segf) != (prevraw & maskf))
  {
    if (segf)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    //BothTft.fillRectangle(x1, y1, SegWidth, SegSize, color);
    xa = x1 + Slant + Slant;
    xb = x1 + Slant;
    ya = y1;
    yb = y1 + SegSize;
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
    BothTft.drawLine(xa++, ya, xb++, yb, color);
  }

  //g
  if ((segg) != (prevraw & maskg))
  {
    if (segg)
    {
      color = ONCOLOR;
    }
    else
    {
      color = OFFCOLOR;
    }
    BothTft.fillRectangle(x1 + Slant, y1 + SegSize, SegSize, SegWidth, color);
  }
}


void R_DrawIndicator(byte indicator, byte state) /* XLS */
{
  byte x1, x2;
  int  y1, y2;
  int  color;
  int  drawc;
  byte good = 1;
  byte circle = 0;

  const __FlashStringHelper *Line1;
  const __FlashStringHelper *Line2 = 0;

  byte L1x;
  byte L2x;
  byte L1y;

  switch (indicator)
  {
    case 0:
      {
        x1 = 0;
        y1 = 0;
        x2 = 92;
        y2 = 60;
        color = GREEN;
        Line1 = F("COMP");
        Line2 = F("ACTY");
        L1x = 18;
        L2x = 18;
        L1y = 15;
        break;
      }

    case 1:
      {
        x1 = 147;
        y1 = 0;
        x2 = 93;
        y2 = 17;
        color = GREEN;
        state = 1;
        Line1 = F("PROG");
        L1x = 18;
        L1y = 2;
        break;
      }

    case 2:
      {
        x1 = 0;
        y1 = 72;
        x2 = 92;
        y2 = 17;
        color = GREEN;
        state = 1;
        Line1 = F("VERB");
        L1x = 18;
        L1y = 2;
        break;
      }

    case 3:
      {
        x1 = 147;
        y1 = 72;
        x2 = 93;
        y2 = 17;
        color = GREEN;
        state = 1;
        Line1 = F("NOUN");
        L1x = 18;
        L1y = 2;
        break;
      }

    // 4,5,6 : three dots at top middle of screen

    case 4:
      {
        x1 = 117;
        y1 = 5;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    case 5:
      {
        x1 = 117;
        y1 = 66;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    case 6:
      {
        x1 = 117;
        y1 = 126;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    // 7,8,9,10 : dots and lines separating verb/noun and R1 R2 R3

    case 7:
      {
        x1 = 10;
        y1 = 145;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    case 8:
      {
        x1 = 20;
        y1 = 144;
        x2 = 205;
        y2 = 3;
        color = GREEN;
        state = 1;
        Line1 = F("");
        L1x = 0;
        break;
      }

    case 9:
      {
        x1 = 20;
        y1 = 149;
        x2 = 205;
        y2 = 3;
        color = WHITE;
        state = 1;
        Line1 = F("");
        L1x = 0;
        break;
      }

    case 10:
      {
        x1 = 230;
        y1 = 145;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    // 11,12,13 : dots and line separating R1 from R2

    case 11:
      {
        x1 = 10;
        y1 = 209;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    case 12:
      {
        x1 = 20;
        y1 = 207;
        x2 = 205;
        y2 = 3;
        color = GREEN;
        state = 1;
        Line1 = F("");
        L1x = 0;
        break;
      }

    case 13:
      {
        x1 = 230;
        y1 = 209;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    // 14,15,16 : dots and line separating R2 from R3

    case 14:
      {
        x1 = 10;
        y1 = 267;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    case 15:
      {
        x1 = 20;
        y1 = 265;
        x2 = 205;
        y2 = 3;
        color = GREEN;
        state = 1;
        Line1 = F("");
        L1x = 0;
        break;
      }

    case 16:
      {
        x1 = 230;
        y1 = 267;
        x2 = 6;
        y2 = 6;
        color = WHITE;
        state = 1;
        Line1 = F("");
        circle = 1;
        L1x = 0;
        break;
      }

    default:
      {
        good = 0;
        break;
      }
  }

  if (good)
  {
    if (state)
    {
      drawc = color;
    }
    else
    {
      drawc = GRAY3;
    }

    if (R_indicatorstatus[indicator] != state)
    {
      if (circle)
      {
        BothTft.fillCircle(x1 + (x2 / 2), y1 + (x2 / 2), x2 / 2, drawc);
      }
      else
      {
        BothTft.fillRectangle(x1, y1, x2, y2, drawc);
      }

      if (Line2)
      {
        DrawText(x1 + L1x, y1 + L1y, Line1);
        DrawText(x1 + L2x, y1 + L1y + 17, Line2);
      }
      else
      {
        DrawText(x1 + L1x, y1 + L1y, Line1);
      }

      R_indicatorstatus[indicator] = state;
    }
  }
}


void R_ClearAllIndicators(byte state = 0) /* XLS */
{
  for (byte i = 0; i < 2; i++)
  {
    R_PV[i] = 128;
    R_VV[i] = 128;
    R_NV[i] = 128;
  }

  for (byte i = 0; i < 6; i++)
  {
    R_AV[i] = 128;
    R_BV[i] = 128;
    R_CV[i] = 128;
  }

  for (byte i = 0; i < 17; i++)
  {
    R_indicatorstatus[i] = 255;
    R_DrawIndicator(i, state);
  }
}


// Qq  COMP ACTY light
// Pxx Display xx under PROG
// Vxx Display xx under VERB
// Nxx Display xx under NOUN
// X+12345 Display +12345 on R1
// Y+12345 Display +12345 on R2
// Z+12345 Display +12345 on R3

// Bug: Qp88v88n88 does not light up prog and verb (Q cannot be too close to P, insert 16 filler chars)


void R_ReadCmd() /* XLS */
{
  byte K, k;

  static byte screen = 0;
  static byte nextb = 0;

  static byte state = 0;

  static byte action;
  static byte dx;
  static int  dy;

  static byte c = 0;
  static byte p = 0;

  static byte buff[6] = {0, 0, 0, 0, 0, 0};

  K = Serial.read() & mySerial.read();
  k = K | 32;

  //order is important
  // do 1st
  if ((screen != 0) && (K == '!'))
  {
    selectedscreen = screen;
    EEPROM.write(0, selectedscreen);

    state = 0;
    screen = 0;
    nextb = 0;

    L_setup();
  }

  // do 2nd
  if ((nextb) && (K != 255))
  {
    screen = 0;
    nextb = 0;
  }

  // do 3rd
  if (K == '[')
  {
    screen = K;
    nextb = 1;
  }

  switch (k)
  {
    case 'q':
      {
        state = 0;
        action = k;
        c = 0;
        p = 0;
        byte COMPON = (K ^ 32) & 32;
        R_DrawIndicator(0, COMPON);
        break;
      }
    case 'p':
      {
        state = 1;
        action = k;
        dx = 155;
        dy = 22;
        c = 2;
        p = 0;
        break;
      }
    case 'v':
      {
        state = 1;
        action = k;
        dx = 17;
        dy = 94;
        c = 2;
        p = 0;
        break;
      }
    case 'n':
      {
        state = 1;
        action = k;
        dx = 155;
        dy = 94;
        c = 2;
        p = 0;
        break;
      }
    case 'x':
      {
        state = 1;
        action = k;
        dx = 10;
        dy = 157;
        c = 6;
        p = 0;
        break;
      }
    case 'y':
      {
        state = 1;
        action = k;
        dx = 10;
        dy = 215;
        c = 6;
        p = 0;
        break;
      }
    case 'z':
      {
        state = 1;
        action = k;
        dx = 10;
        dy = 273;
        c = 6;
        p = 0;
        break;
      }
  }

  switch (state)
  {
    case 0:
      {
        break;
      }

    case 1:
      {
        state = 2;
        break;
      }

    case 2:
      {
        if (K != 255)
        {
          buff[p++] = K;

          if (--c == 0)
          {
            state = 3;
          }
        }
        break;
      }

    case 3:
      {
        byte pv;

        for (c = 0; c < p; c++)
        {
          switch (action)
          {
            case 'p':
              {
                pv = R_PV[c];
                R_PV[c] = buff[c];
                break;
              }
            case 'v':
              {
                pv = R_VV[c];
                R_VV[c] = buff[c];
                break;
              }
            case 'n':
              {
                pv = R_NV[c];
                R_NV[c] = buff[c];
                break;
              }
            case 'x':
              {
                pv = R_AV[c];
                R_AV[c] = buff[c];
                break;
              }
            case 'y':
              {
                pv = R_BV[c];
                R_BV[c] = buff[c];
                break;
              }
            case 'z':
              {
                pv = R_CV[c];
                R_CV[c] = buff[c];
                break;
              }
          }

          if ((action > 'w') && (action < 'z' + 1) && (c == 0))
          {
            R_DrawSign(dx, dy, buff[c], pv);
            dx += 25;
          }
          else
          {
            R_Draw7Seg(dx, dy, buff[c] - '0', pv - '0');
            dx += 39;
          }
        }

        state = 0;
        break;
      }
  }
}


void R_setup() /* XLS */
{
  BothTft.fillRectangle(0, 0, 240, 320, BLACK);
  R_ClearAllIndicators();
}

