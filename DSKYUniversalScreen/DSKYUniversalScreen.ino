// DSKYUniversalScreen.ino
// @arduinoenigma 2021
//
// this sketch runs on both Arduino Nanos running the left and right lcd screens
// contains code for left screen: warnings and right screen: numeric data


#define HWSERIALSPEED 9600
#define SWSERIALSPEED 38400
#define rxPin 8
#define txPin 9

#define TFTSeed

#define GRAY3 0x18c3
#define GRAY4 0x3186
#define GRAY5 0x4a49

#define GRAY75 0xBDF7
#define GRAY65 0x94B2
#define GRAY55 0x6B8D

#define AVYELLOW 64480

#include <SPI.h>
#include <FastTftILI9341.h>

#include <SoftwareSerial.h>

#include <EEPROM.h>
#include <avr/eeprom.h>

PDQ_ILI9341 BothTft;

SoftwareSerial mySerial(rxPin, txPin); // RX, TX

byte selectedscreen = 0;


void setup()  /* XLS */
{
  // put your setup code here, to run once:
  Serial.begin(HWSERIALSPEED);
  Serial.println(F("DSKY Universal Screen 07182021"));

  //TFT initialization routine:
  BothTft.TFTinit();  //init TFT library

  //Software Serial must be init after TFT Library...
  pinMode(rxPin, INPUT);
  mySerial.begin(SWSERIALSPEED);

  selectedscreen = EEPROM.read(0); //BUG: enable

  if ((selectedscreen != '[') && (selectedscreen != ']'))
  {
    BothTft.fillRectangle(0, 0, 240, 320, RED);
    BothTft.fillTriangle(120, 50, 10, 270, 230, 270, BLACK);
    BothTft.fillTriangle(120, 75, 30, 255, 210, 255, YELLOW);
    BothTft.drawChar('!', 68, 104, 21, BLACK);

    do
    {
      byte K;
      K = Serial.read() & mySerial.read(); // empty char returns 255, (&) mixes actual read char with empty

      if ((K == '[') || (K == ']'))
      {
        selectedscreen = K;
      }
    } while ((selectedscreen != '[') && (selectedscreen != ']'));

    EEPROM.write(0, selectedscreen);
  }

  switch (selectedscreen)
  {
    case '[':
      {
        L_setup();
        break;
      }

    case ']':
      {
        R_setup();
        break;
      }
  }
}


void loop()  /* XLS */
{
  // put your main code here, to run repeatedly:

  switch (selectedscreen)
  {
    case '[':
      {
        L_ReadCmd();
        break;
      }

    case ']':
      {
        R_ReadCmd();
        break;
      }
  }
}
